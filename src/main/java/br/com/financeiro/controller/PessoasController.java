package br.com.financeiro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.financeiro.model.Pessoa;

@Controller
public class PessoasController {

	@RequestMapping("/pessoa/CadastroPessoa")
	public String novo() {
		return "pessoa/CadastroPessoa";
	}
	
	@RequestMapping(value = "/pessoa/CadastroPessoa", method = RequestMethod.POST)
	public String cadastrar(Pessoa pessoa) {
		System.out.println(">>> Cadastrar <<<" + pessoa.getNome());
		return "pessoa/CadastroPessoa";
	}
}
