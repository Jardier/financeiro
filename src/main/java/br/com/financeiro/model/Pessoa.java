package br.com.financeiro.model;

import lombok.Data;

public @Data class Pessoa {
	
	private String nome;
	private int idade;

}
